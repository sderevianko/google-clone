package com.example.google.clone.entity;

import org.apache.lucene.queryparser.classic.ParseException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class WebPageIndexTest {
    private static final int NUMBER_OF_FILES = 7;
    private static final String QUERY = "text";
    private final Set<WebPage> pages = new HashSet<>();
    private final List<WebPage> testWebPages = new ArrayList<>();
    private WebPageIndex webPageIndex;
    private LuceneIndex luceneIndex;

    @BeforeEach
    void setUp() {
        luceneIndex = new LuceneIndex();
        LuceneSearch luceneSearch = new LuceneSearch();
        webPageIndex = new WebPageIndex(luceneIndex, luceneSearch);
        Set<String> urls = new HashSet<>();
        urls.add("url");
        WebPage webPage = new WebPage("base url", "title", "text of page", urls);
        WebPage page = new WebPage("base url 2", "title 2", "text of page 2", urls);
        pages.add(webPage);
        pages.add(page);
        testWebPages.add(page);
        testWebPages.add(webPage);
    }

    @Test
    void indexPages() throws IOException {
        webPageIndex.indexPages(pages);
        String[] strings = luceneIndex.getMemoryIndex().listAll();
        assertThat(strings.length).isEqualTo(NUMBER_OF_FILES);
    }

    @Test
    void searchPages() throws ParseException, IOException {
        webPageIndex.indexPages(pages);
        List<WebPage> webPages = webPageIndex.searchPages(pages, QUERY);
        assertThat(webPages).isEqualTo(testWebPages);
    }
}
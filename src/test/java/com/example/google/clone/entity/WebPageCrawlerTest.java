package com.example.google.clone.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class WebPageCrawlerTest {
    private static final String TEST_URL = "https://hi-news.ru/research-development/apparat-kyuriositi-izuchil-" +
            "glinyanuyu-poverxnost-marsa-i-nashel-priznaki-vody.html";
    private static final int COUNT_OF_LINKS = 41;
    private static final int COUNT_OF_PAGES = 41;
    private static final int DEEP_OF_SEARCHING = 1;
    private WebPageCrawler webPageCrawler;

    @BeforeEach
    void setUp() {
        ConnectionCreator connectionCreator = new ConnectionCreator();
        JsoupWebParser jsoupWebParser = new JsoupWebParser(connectionCreator);
        webPageCrawler = new WebPageCrawler(jsoupWebParser);
    }

    @Test
    void getAllURLS() {
        assertThat(webPageCrawler.getAllURLS(TEST_URL, DEEP_OF_SEARCHING).size()).isEqualTo(COUNT_OF_LINKS);
    }

    @Test
    void getAllWebPage() {
        Set<String> urls = webPageCrawler.getAllURLS(TEST_URL, DEEP_OF_SEARCHING);
        assertThat(webPageCrawler.getAllWebPage(urls).size()).isEqualTo(COUNT_OF_PAGES);
    }
}
package com.example.google.clone.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class LuceneIndexTest {
    private static final int NUMBER_OF_FILES = 4;
    private LuceneIndex luceneIndex;
    private WebPage webPage;

    @BeforeEach
    void setUp() {
        luceneIndex = new LuceneIndex();
        Set<String> urls = new HashSet<>();
        urls.add("url");
        webPage = new WebPage("base url", "title", "text of page", urls);
    }

    @Test
    void createIndex() throws IOException {
        luceneIndex.createIndex(webPage);
        String[] strings = luceneIndex.getMemoryIndex().listAll();
        assertThat(strings.length).isEqualTo(NUMBER_OF_FILES);
    }
}
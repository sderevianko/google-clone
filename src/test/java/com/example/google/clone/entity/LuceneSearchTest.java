package com.example.google.clone.entity;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.queryparser.classic.ParseException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class LuceneSearchTest {
    private LuceneSearch luceneSearch;
    private LuceneIndex luceneIndex;
    private WebPage webPage;

    @BeforeEach
    void setUp() throws IOException {
        luceneIndex = new LuceneIndex();
        luceneSearch = new LuceneSearch();
        Set<String> urls = new HashSet<>();
        urls.add("url");
        webPage = new WebPage("base url", "title", "text of page", urls);
        luceneIndex.createIndex(webPage);
    }

    @Test
    void searchQuery() throws ParseException, IOException {
        Set<Document> documents = luceneSearch.searchQuery(
                webPage,
                "text",
                luceneIndex.getMemoryIndex(),
                luceneIndex.getAnalyzer()
        );
        Set<Document> testDocuments = new HashSet<>();

        Document document = new Document();
        document.add(new TextField("base url", "text of page", Field.Store.YES));
        testDocuments.add(document);

        assertThat(documents.toString()).isEqualTo(testDocuments.toString());
    }

    @Test
    void getSearchPages() {
        Set<Document> testDocuments = new HashSet<>();
        Set<WebPage> webPages = new HashSet<>();

        Document document = new Document();
        document.add(new TextField("base url", "text of page", Field.Store.YES));
        testDocuments.add(document);
        webPages.add(webPage);

        List<WebPage> testPages = new ArrayList<>();
        testPages.add(webPage);

        assertThat(luceneSearch.getSearchPages(testDocuments, webPages)).isEqualTo(testPages);
    }
}
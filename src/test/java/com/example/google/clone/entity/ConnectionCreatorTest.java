package com.example.google.clone.entity;

import org.jsoup.nodes.Document;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class ConnectionCreatorTest {
    private static final String URL = "https://hi-news.ru/";
    private ConnectionCreator connectionCreator;

    @BeforeEach
    void setUp() {
        connectionCreator = new ConnectionCreator();
    }

    @Test
    void getHTMLOfPage() {
        Document document = connectionCreator.getHTMLOfPage(URL);
        assertNotNull(document);
    }
}
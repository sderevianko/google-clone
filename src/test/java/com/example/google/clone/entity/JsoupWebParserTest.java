package com.example.google.clone.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class JsoupWebParserTest {
    private static final String TEST_URL = "https://hi-news.ru/research-development/apparat-kyuriositi-izuchil-" +
            "glinyanuyu-poverxnost-marsa-i-nashel-priznaki-vody.html";
    private static final String TEST_TITLE = "Аппарат «Кьюриосити» изучил глиняную поверхность Марса" +
            " и нашел признаки воды - Hi-News.ru";
    private static final int COUNT_OF_LINKS = 40;
    private JsoupWebParser jsoupWebParser;

    @BeforeEach
    void setUp() {
        ConnectionCreator connectionCreator = new ConnectionCreator();
        jsoupWebParser = new JsoupWebParser(connectionCreator);
    }

    @Test
    void getAllTextOfPageWithoutTags() {
        String text = jsoupWebParser.getAllTextOfPageWithoutTags(TEST_URL);
        assertNotNull(text);
    }

    @Test
    void getAllLinksOfPage() {
        assertThat(jsoupWebParser.getAllLinksOfPage(TEST_URL).size()).isEqualTo(COUNT_OF_LINKS);
    }

    @Test
    void getTitleOfPage() {
        String title = jsoupWebParser.getTitleOfPage(TEST_URL);
        assertThat(title).isEqualTo(TEST_TITLE);
    }
}
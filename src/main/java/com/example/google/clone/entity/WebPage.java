package com.example.google.clone.entity;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Set;

public class WebPage {
    @NotNull
    @NotEmpty
    private final String url;
    @NotNull
    private final String title;
    @NotNull
    private final String textOfPage;
    @NotNull
    private final Set<String> urls;

    public WebPage(String url, String title, String textOfPage, Set<String> urls) {
        this.url = url;
        this.title = title;
        this.textOfPage = textOfPage;
        this.urls = urls;
    }

    public String getUrl() {
        return url;
    }

    public String getTitle() {
        return title;
    }

    public String getTextOfPage() {
        return textOfPage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WebPage webPage = (WebPage) o;
        return url.equals(webPage.url) && title.equals(webPage.title) && textOfPage.equals(webPage.textOfPage) && urls.equals(webPage.urls);
    }

    @Override
    public int hashCode() {
        return Objects.hash(url, title, textOfPage, urls);
    }

    @Override
    public String toString() {
        return "WebPage{" +
                "url='" + url + '\'' +
                ", title='" + title + '\'' +
                ", textOfPage='" + textOfPage + '\'' +
                ", urls=" + urls +
                '}';
    }
}

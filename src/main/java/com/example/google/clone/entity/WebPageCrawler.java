package com.example.google.clone.entity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Logger;

@Component
public class WebPageCrawler {
    private static final Logger LOG = Logger.getLogger(WebPageCrawler.class.getName());
    private static final int NUMBER_OF_THREADS = 4;
    private final JsoupWebParser jsoupWebParser;
    private Set<WebPage> webPages;

    @Autowired
    public WebPageCrawler(JsoupWebParser jsoupWebParser) {
        this.jsoupWebParser = jsoupWebParser;
    }

    public Set<String> getAllURLS(String url, int deepOfSearching) {
        ExecutorService executorService = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
        Set<String> urls = Collections.synchronizedSet(new HashSet<>());
        urls.add(url);
        for (int i = 1; i <= deepOfSearching; i++) {
            Set<String> allLinksOfPage = Collections.synchronizedSet(new HashSet<>());
            for (String str : urls) {
                Future<Set<String>> future = executorService.submit(() -> jsoupWebParser.getAllLinksOfPage(str));

                try {
                    allLinksOfPage.addAll(future.get());
                } catch (InterruptedException | ExecutionException e) {
                    LOG.info("ExecutorService exception");
                } catch (NullPointerException exception) {
                    LOG.info("NullPointerException exception");
                }
            }
            urls.addAll(allLinksOfPage);
        }
        executorService.shutdown();

        return urls;
    }

    public Set<WebPage> getAllWebPage(Set<String> urls) {
        ExecutorService executorService = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
        webPages = Collections.synchronizedSet(new HashSet<>());

        for (String url : urls) {
            Future<WebPage> future = executorService.submit(() -> getWebPage(url));
            try {
                webPages.add(future.get());
            } catch (InterruptedException | ExecutionException e) {
                LOG.info("ExecutorService exception");
            } catch (NullPointerException exception) {
                LOG.info("NullPointerException exception");
            }
        }
        executorService.shutdown();

        return webPages;
    }

    public Set<WebPage> getWebPages() {
        return webPages;
    }

    private WebPage getWebPage(String url) {

        return new WebPage(
                url,
                jsoupWebParser.getTitleOfPage(url),
                jsoupWebParser.getAllTextOfPageWithoutTags(url),
                jsoupWebParser.getAllLinksOfPage(url)
        );
    }
}

package com.example.google.clone.entity;

import org.apache.lucene.document.Document;
import org.apache.lucene.queryparser.classic.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class WebPageIndex {
    private static final int QUANTITY_OF_WEBPAGES = 10;
    private final LuceneIndex luceneIndex;
    private final LuceneSearch luceneSearch;

    @Autowired
    public WebPageIndex(LuceneIndex luceneIndex, LuceneSearch luceneSearch) {
        this.luceneIndex = luceneIndex;
        this.luceneSearch = luceneSearch;
    }

    public void indexPages(Set<WebPage> pages) throws IOException {
        for (WebPage page : pages) {
            luceneIndex.createIndex(page);
        }
    }

    public List<WebPage> searchPages(Set<WebPage> pages, String query) throws ParseException, IOException {
        Set<Document> documents = new HashSet<>();
        for (WebPage page : pages) {
            documents.addAll(luceneSearch.searchQuery(page, query, luceneIndex.getMemoryIndex(), luceneIndex.getAnalyzer()));
        }
        List<WebPage> searchPages = luceneSearch.getSearchPages(documents, pages);

        return searchPages.size() > QUANTITY_OF_WEBPAGES ? searchPages.subList(0, QUANTITY_OF_WEBPAGES) : searchPages;
    }
}

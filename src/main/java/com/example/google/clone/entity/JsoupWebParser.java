package com.example.google.clone.entity;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Component
public class JsoupWebParser {
    private final ConnectionCreator connectionCreator;

    @Autowired
    public JsoupWebParser(ConnectionCreator connectionCreator) {
        this.connectionCreator = connectionCreator;
    }

    public String getAllTextOfPageWithoutTags(String url) {
        return connectionCreator.getHTMLOfPage(url).text();
    }

    public Set<String> getAllLinksOfPage(String url) {
        Set<String> urlList = Collections.synchronizedSet(new HashSet<>());
        Document document = connectionCreator.getHTMLOfPage(url);
        Element body = document.body();
        if (body != null) {
            Elements urls = body.select("a");
            for (Element element : urls) {
                String str = element.absUrl("href");
                if ((str != null) && (!str.isEmpty())) {
                    urlList.add(str);
                }
            }
        }

        return urlList;
    }

    public String getTitleOfPage(String url) {
        return connectionCreator.getHTMLOfPage(url).title();
    }
}

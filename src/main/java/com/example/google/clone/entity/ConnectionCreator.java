package com.example.google.clone.entity;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

import javax.net.ssl.SSLHandshakeException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.logging.Logger;

@Service
public class ConnectionCreator {
    private static final Logger LOG = Logger.getLogger(ConnectionCreator.class.getName());

    public Document getHTMLOfPage(String url) {
        Document document = new Document("hi-news.ru");

        try {
            document = Jsoup.connect(url)
                    .userAgent("Chrome/4.0.249.0 Opera/532.5")
                    .referrer("https://www.google.com")
                    .get();
        } catch (SSLHandshakeException exception) {
            LOG.info("SSLHandshakeException");
        } catch (MalformedURLException except) {
            LOG.info("MalformedURLException");
        } catch (IOException e) {
            LOG.info("Connection failed");
        }

        return document;
    }
}

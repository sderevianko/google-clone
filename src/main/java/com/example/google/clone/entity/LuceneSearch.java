package com.example.google.clone.entity;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class LuceneSearch {

    public Set<Document> searchQuery(WebPage webPage,
                                     String queryString,
                                     Directory memoryIndex,
                                     StandardAnalyzer analyzer) throws ParseException, IOException {
        Set<Document> documents;
        Query query = new QueryParser(webPage.getUrl(), analyzer).parse(queryString);
        IndexReader indexReader = DirectoryReader.open(memoryIndex);
        IndexSearcher searcher = new IndexSearcher(indexReader);
        TopDocs topDocs = searcher.search(query, 1);
        documents = new HashSet<>();
        for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
            documents.add(searcher.doc(scoreDoc.doc));
        }

        return documents;
    }

    public List<WebPage> getSearchPages(Set<Document> documents, Set<WebPage> webPages) {
        Set<String> urls = new HashSet<>();
        List<WebPage> searchPages = new ArrayList<>();
        for (Document document : documents) {
            for (int i = 0; i < document.getFields().size(); i++) {
                urls.add(document.getFields().get(i).name());
            }
        }

        for (String url : urls) {
            webPages.stream().filter(webPage -> url.equals(webPage.getUrl())).forEach(searchPages::add);
        }

        return searchPages;
    }
}

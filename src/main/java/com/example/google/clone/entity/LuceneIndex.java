package com.example.google.clone.entity;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.ByteBuffersDirectory;
import org.apache.lucene.store.Directory;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class LuceneIndex {
    private final Directory memoryIndex = new ByteBuffersDirectory();
    private final StandardAnalyzer analyzer = new StandardAnalyzer();

    public void createIndex(WebPage page) throws IOException {
        IndexWriterConfig indexWriterConfig = new IndexWriterConfig(analyzer);
        try (IndexWriter writer = new IndexWriter(memoryIndex, indexWriterConfig)) {
            Document document = new Document();
            document.add(new TextField(page.getUrl(), page.getTextOfPage(), Field.Store.YES));
            writer.addDocument(document);
        }
    }

    public Directory getMemoryIndex() {
        return memoryIndex;
    }

    public StandardAnalyzer getAnalyzer() {
        return analyzer;
    }
}

package com.example.google.clone.controller;

import com.example.google.clone.controller.dto.WebPageDTO;
import com.example.google.clone.service.IndexService;
import com.example.google.clone.service.SearchService;
import org.apache.lucene.queryparser.classic.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.List;

@Controller
public class SearchController {
    private final SearchService searchService;
    private final IndexService indexService;

    @Autowired
    public SearchController(SearchService searchService, IndexService indexService) {
        this.searchService = searchService;
        this.indexService = indexService;
    }

    @GetMapping("/")
    public ModelAndView search() {
        return new ModelAndView("search.html");
    }

    @PostMapping("/search")
    public ModelAndView getSearchingURL(@RequestParam(value = "query") String query) throws IOException, ParseException {
        ModelAndView modelAndView = new ModelAndView("searchresult.html");
        modelAndView.addObject("query", query);

        List<WebPageDTO> pages = searchService.searchQuery(indexService.getWebPages(), query);

        if (pages.isEmpty()) {
            modelAndView.addObject("message", "Not found any coincidence");
        }
        modelAndView.addObject("items", pages);

        return modelAndView;
    }
}

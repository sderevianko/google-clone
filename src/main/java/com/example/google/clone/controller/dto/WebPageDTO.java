package com.example.google.clone.controller.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Objects;

public class WebPageDTO {
    @NotNull
    @NotEmpty
    private final String url;
    @NotNull
    private final String title;

    public WebPageDTO(String url, String title) {
        this.url = url;
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WebPageDTO webPageDTO = (WebPageDTO) o;
        return url.equals(webPageDTO.url) && title.equals(webPageDTO.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(url, title);
    }

    @Override
    public String toString() {
        return "WebPageDTO{" +
                "url='" + url + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}

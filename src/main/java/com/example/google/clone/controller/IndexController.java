package com.example.google.clone.controller;

import com.example.google.clone.entity.WebPage;
import com.example.google.clone.service.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Set;

@Controller
@RequestMapping("/index")
public class IndexController {
    private static final int DEEP_OF_SEARCHING = 3;
    private final IndexService indexService;

    @Autowired
    public IndexController(IndexService indexService) {
        this.indexService = indexService;
    }

    @GetMapping
    public ModelAndView getIndexView() {
        return new ModelAndView("index.html");
    }

    @PostMapping
    public ModelAndView getUrl(@RequestParam(value = "Link") String url) {
        ModelAndView modelAndView = new ModelAndView("indexresult.html");
        modelAndView.addObject("Link", url);

        Set<WebPage> webPages = indexService.getAllItem(indexService.getAllLinks(url, DEEP_OF_SEARCHING));
        String message = webPages.size() + " link was indexed";

        modelAndView.addObject("message", message);
        modelAndView.addObject("items", webPages);

        return modelAndView;
    }
}

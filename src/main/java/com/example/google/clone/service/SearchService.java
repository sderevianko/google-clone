package com.example.google.clone.service;

import com.example.google.clone.controller.dto.WebPageDTO;
import com.example.google.clone.entity.WebPage;
import com.example.google.clone.entity.WebPageIndex;
import org.apache.lucene.queryparser.classic.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class SearchService {
    private final WebPageIndex webPageIndex;

    @Autowired
    public SearchService(WebPageIndex webPageIndex) {
        this.webPageIndex = webPageIndex;
    }

    public List<WebPageDTO> searchQuery(Set<WebPage> pages, String query) throws ParseException, IOException {
        webPageIndex.indexPages(pages);
        List<WebPage> webPages = webPageIndex.searchPages(pages, query);

        return convertWebPageToDTOS(webPages);
    }

    private List<WebPageDTO> convertWebPageToDTOS(List<WebPage> webPages) {
        List<WebPageDTO> pageDTOS = new ArrayList<>();
        for (WebPage webPage : webPages) {
            pageDTOS.add(new WebPageDTO(webPage.getUrl(), webPage.getTitle()));
        }

        return pageDTOS;
    }
}

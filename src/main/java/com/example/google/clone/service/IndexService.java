package com.example.google.clone.service;

import com.example.google.clone.entity.WebPage;
import com.example.google.clone.entity.WebPageCrawler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class IndexService {
    private final WebPageCrawler webPageCrawler;

    @Autowired
    public IndexService(WebPageCrawler webPageCrawler) {
        this.webPageCrawler = webPageCrawler;
    }

    public Set<String> getAllLinks(String url, int deepOfSearching) {
        return webPageCrawler.getAllURLS(url, deepOfSearching);
    }

    public Set<WebPage> getAllItem(Set<String> urls) {
        return webPageCrawler.getAllWebPage(urls);
    }

    public Set<WebPage> getWebPages() {
        return webPageCrawler.getWebPages();
    }
}
